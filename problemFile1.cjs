const http = require('http');
const uuid = require('uuid');
const { port , statuscode } = require('./config');


http.createServer((request, response) => {                                                                                              //created server
  if (request.url.toLowerCase() === '/html') {                                                                                          //checking if url is an html
    response.setHeader('Content-Type', 'text/html');
    response.end(`<!DOCTYPE html>
    <html>
      <head>
      </head>
      <body>
          <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
          <p> - Martin Fowler</p>
   
      </body>
    </html>
   `);                                                                                                                                  //ends the response
  }
  else if (request.url === '/json') {                                                                                                   //checks if the url is of json type
    response.setHeader('Content-Type', 'application/json');
    response.end(JSON.stringify(`
    "slideshow": {
      "author": "Yours Truly",
      "date": "date of publication",
      "slides": [
        {
          "title": "Wake up to WonderWidgets!",
          "type": "all"
        },
        {
          "items": [
            "Why <em>WonderWidgets</em> are great",
            "Who <em>buys</em> WonderWidgets"
          ],
          "title": "Overview",
          "type": "all"
        }
      ],
      "title": "Sample Slide Show"
    }
  `));                                                                                                                                  //ends the response
  }
  else if (request.url.toLowerCase() === '/uuid') {                                                                                     //checks if the url is of uuid type

    let uuidNew = uuid.v4();                                                                                                            //uuid module is used for using function v4().
    response.setHeader('Content-Type', 'application/json');
    let finalOutput = JSON.stringify({ UUID: uuidNew });
    response.write(finalOutput);
    response.end();                                                                                                                     //ends response
  }
  else if (request.url.toLowerCase().includes('/status')) {                                                                             //checks if the url is of status type
    let statuscodeFromUrl = request.url.split('/');                                                                                     //splitting the url
    statuscodeFromUrl = +(statuscodeFromUrl[statuscodeFromUrl.length - 1]);                                                             //extracting the status code from split array
    response.setHeader('Content-Type', 'application/json');
    if (isNaN(statuscodeFromUrl) === false ) {
      if (statuscodeFromUrl in http.STATUS_CODES && statuscodeFromUrl != 100) {
        response.statusCode = statuscodeFromUrl;                                                                                        //writing to the status code
        response.end((`{ "Status_Code": "${statuscodeFromUrl}" }`));
      }
      else {
        response.statusCode = 400;                                                                                                      //writing to the status code
        response.end(JSON.stringify({ Status_Code: "Invalid: Bad request" }));
      }
    }
    else{
      response.end(JSON.stringify({"Error " : "Wrong format for status input, use numeric value for the same. Retry! Viable format is /status/status-code"}));
    }
  }
  else if (request.url.toLowerCase().includes('/delay')) {                                                                              //condition to check if url is of delay type
    let delaycodeFromUrl = request.url.split('/');                                                                                      //splitting url to get time for delay in seconds
    delaycodeFromUrl = delaycodeFromUrl[delaycodeFromUrl.length - 1];
    response.setHeader('Content-Type', 'application/json');
    if (delaycodeFromUrl >= 0 && isNaN(delaycodeFromUrl) === false) {
      response.write(JSON.stringify({"Wait Time " :  + delaycodeFromUrl + " seconds."}));
      setTimeout(() => {
        response.statusCode = statuscode;                                                                                               //assigning statusCode as 200;environment variable used
        response.end(JSON.stringify({ Status_code: statuscode }));                                                                      //displaying status code
      }, delaycodeFromUrl * 1000);                                                                                                      //defined delay in seconds
    }
    else {
      response.end(JSON.stringify({"Error" : "Invalid time input, please put positive time (in seconds) and it should be a numeric value. Retry! Viable format is /delay/delay-time"}));
    }
  }
  else {
    response.setHeader('Content-Type', 'text/html');
    response.end(`<!DOCTYPE html>
    <html>
      <head>
      </head>
      <body>
          <h1>Input proper URL.</h1>
          <h3>Try using the following:</h3>
          <h5>/html</h5>
          <h5>/json</h5>
          <h5>/uuid</h5>
          <h5>/status/status code</h5>
          <h5>/delay/delay time in seconds</h5>
      </body>
    </html>
    `);                                                                                                          //default output if no suiting URL found
  }
}).listen(port, (err) => {
  if (err) {
    console.error(err);
  }
  else {
    console.log("Server running");
  }
});