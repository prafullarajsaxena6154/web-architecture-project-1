const dotenv = require('dotenv');
dotenv.config();
module.exports = {
    port: process.env.PORT,
    statuscode: process.env.STATUSCODE,
    jsoncode: process.env.JSONOUTPUT,
    htmlcode: process.env.HTMLOUTPUT,
    defaultcode: process.env.DEFAULTOUTPUT
  };
  